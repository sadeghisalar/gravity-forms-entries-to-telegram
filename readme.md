### Gravity Forms Entries To Telegram
with this plugin, you can send the entry of your forms to your channels in Telegram.
Easily select your forms, enter the channel ID and activate the fields you want to send,
Then get the messages in your Telegram chats

### Installation 
1. Upload the plugin files to the `/wp-content/plugins/gravityforms-telegram` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the tools menu screen to configure the plugin

###  Wordpress Repository
also, you can install it from [wordpress repository](https://wordpress.org/plugins/send-form-entries-to-telegram/)


### Frequently Asked Questions
**can i use this plugin in other form builders?**
*no, this plugin only work on gravityforms*

**how can i found configuration page ?**
*you can find plugin configuration in the tools menu*

**how can i use this?**
*go to plugin configuration page, Easily select your forms, enter the channel ID and activate the fields you want to send,*

**how this plugin work?**
*when user submit a form, this plugin send entry to your defined Chat on the telegram.*

### Screenshots
![](./screenshots/screenshot-1.PNG)

![](./screenshots/screenshot-2.PNG)
